import mongoose from 'mongoose';

mongoose.Promise = global.Promise;

const database = mongoose.connect('mongodb://TestUnitaire:TestUnitaire1@ds149596.mlab.com:49596/moustanir', {
    useMongoClient: true,
    socketTimeoutMS: 0,
    keepAlive: true,
    reconnectTries: 30,
    useNewUrlParser: true,
    useCreateIndex: true
});

module.exports = database;