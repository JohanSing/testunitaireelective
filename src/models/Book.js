import mongoose from 'mongoose';

const BookSchema = mongoose.Schema({
    title: String,
    years: Number,
    pages: Number,
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    }
});

const Book = mongoose.model('Book', BookSchema);

module.exports = Book;
