import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import chaiNock from 'chai-nock';
import chaiAsPromised from 'chai-as-promised';
import path from 'path';
import nock from 'nock';

import server from '../server';
import Book from '../models/Book';

chai.use(chaiHttp);
chai.use(chaiNock);
chai.use(chaiAsPromised);

describe('Integration tests for Book resources', () => {

    beforeEach(() => {
        Book.deleteMany({}, (err, res) => {
            if (err) return;
            else return;
        });
    });

    it('retrieve an empty list of books', (done) => {
        chai.request(server)
            .get('/book')
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.a('array');
                expect(res.body.length).to.equal(0);
                done();
            });
    });

    it('post a book', (done) => {
        chai.request(server)
            .post('/book')
            .set('content-type', 'application/json')
            .send({
                'title': 'One Punch Man',
                'years': 1990,
                'pages': 400
            })
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.body.message).to.equal('Book successfully added');
                expect(res.body.data).to.be.a('object');
                expect(res.body.data.title).to.be.a('string');
                expect(res.body.data.title).to.equal('One Punch Man');
                expect(res.body.data.years).to.be.a('Number');
                expect(res.body.data.years).to.equal(1990);
                expect(res.body.data.pages).to.be.a('Number');
                expect(res.body.data.pages).to.equal(400);
                done();
            });
    });

    it('retrieve a book with ID', (done) => {
        let book = new Book({ title: "One Punch Man", years: 1995, pages: 125 });

        book.save((err,data) => {
            
            chai.request(server)
                .get(`/book/${data._id}`)
                .end((err, res) => {
                    expect(err).to.be.null;
                    expect(res).to.have.status(200);
                    expect(res.body.message).to.equal('Book fetched');
                    expect(res.body).to.be.a('object');
                    expect(res.body.data.title).to.be.a('string');
                    expect(res.body.data.title).to.equal('One Punch Man');
                    expect(res.body.data.years).to.be.a('Number');
                    expect(res.body.data.years).to.equal(1995);
                    expect(res.body.data.pages).to.be.a('Number');
                    expect(res.body.data.pages).to.equal(125);
                    done();
                });

        });
    });

    it('put a book with ID', (done) => {
        let book = new Book({ title: "One Punch Man", years: 1995, pages: 125 });
        book.save((err,data) => {
            
            chai.request(server)
                .put(`/book/${data._id}`)
                .set('content-type', 'application/json')
                .send({
                    'title': 'Mob Psycho 100',
                    'years': 1990,
                    'pages': 400
                })
                .end((err, res) => {
                    expect(err).to.be.null;
                    expect(res).to.have.status(200);
                    expect(res.body.message).to.equal('Book successfully updated');
                    expect(res.body.data).to.be.a('object');
                    expect(res.body.data.title).to.be.a('string');
                    expect(res.body.data.title).to.equal('Mob Psycho 100');
                    expect(res.body.data.years).to.be.a('Number');
                    expect(res.body.data.years).to.equal(1990);
                    expect(res.body.data.pages).to.be.a('Number');
                    expect(res.body.data.pages).to.equal(400);
                    done();
                });

        });
    });

    it('delete a book', (done) => {
        let book = new Book({ title: "One Punch Man", years: 1995, pages: 125 });

        book.save((err,data) => {
            
            chai.request(server)
                .delete(`/book/${data._id}`)
                .end((err, res) => {
                    expect(err).to.be.null;
                    expect(res).to.have.status(200);
                    expect(res.body.message).to.equal('Book successfully deleted');
                    done();
                });

        });

    });

});

it('should respond 200 on GET', done => {
    nock('http://localhost:8080')
        .get('/book')
        .reply(200, {"books":[]})

    chai.request('http://localhost:8080')
    .get('/book')
    .end((err, res) => {
        if (err) console.log(err);
        expect(res.body.books).to.be.a('array');
        expect(res).to.have.status(200);
        done();
    })
})

it('should respond 200 on POST', done => {
    nock('http://localhost:8080')
        .post('/book')
        .reply(200, {message:'book successfully added'})

    chai.request('http://localhost:8080')
    .post('/book')
    .send({ id: '1234-test-5678', title: 'LeClubDes5', years: 2019, pages: 1234})
    .end((err, res) => {
        if (err) console.log(err);
        expect(res).to.have.status(200);
        expect(res.body.message).to.equal('book successfully added');
        done();
    })
})

it('should respond 200 on PUT', done => {
    let book = new Book({id: 123, title: 'LeClubDes7', years: 2017, pages: 1235});

        nock('http://localhost:8080')
        .put('/book/' + book._id)
        .reply(200, {message: 'book successfully updated'})

        chai.request('http://localhost:8080')
        .put('/book/' + book._id)
        .send({ title: 'LeClubDes7', years: 2017, pages: 1235})
        .end((err, res) => {
            if (err) console.log(err);
            expect(res).to.have.status(200);
            expect(res.body.message).to.equal('book successfully updated');
            done();
        })
})

it('should respond 200 on DELETE', done => {
    let book = new Book({id: 123, title: 'LeClubDes7', years: 2017, pages: 1235});

        nock('http://localhost:8080')
        .delete('/book/' + book._id)
        .reply(200, {message: 'book successfully deleted'})

        chai.request('http://localhost:8080')
        .delete('/book/' + book._id)
        .end((err, res) => {
            if (err) console.log(err);
            expect(res).to.have.status(200);
            expect(res.body.message).to.equal('book successfully deleted');
            done();
        })
})

it('should respond 400 on GET', done => {
    nock('http://localhost:8080')
        .get('/book')
        .reply(400, {message: 'error fetching books'})

    chai.request('http://localhost:8080')
    .get('/book')
    .end((err, res) => {
        expect(res.body.message).to.equal('error fetching books');
        expect(res).to.have.status(400);
        done();
    })
})

it('should respond 400 on POST', done => {
    nock('http://localhost:8080')
        .post('/book')
        .reply(400, {message:'error adding the book'})

    chai.request('http://localhost:8080')
    .post('/book')
    .send({ id: '1234-test-5678', title: 'LeClubDes5', years: 2019, pages: 1234})
    .end((err, res) => {
        expect(res).to.have.status(400);
        expect(res.body.message).to.equal('error adding the book');
        done();
    })
})

it('should respond 400 on PUT', done => {
    let book = new Book({title: 'LeClubDes7', years: 2017, pages: 1235});

        nock('http://localhost:8080')
        .put('/book/' + book._id)
        .reply(400, {message: 'error updating the book'})

        chai.request('http://localhost:8080')
        .put('/book/' + book._id)
        .send({ title: 'LeClubDes7', years: 2017, pages: 1235})
        .end((err, res) => {
            expect(res).to.have.status(400);
            expect(res.body.message).to.equal('error updating the book');
            done();
        })
})

it('should respond 400 on DELETE', done => {
    let book = new Book({title: 'LeClubDes7', years: 2017, pages: 1235});

        nock('http://localhost:8080')
        .delete('/book/' + book._id)
        .reply(400, {message: 'error deleting the book'})

        chai.request('http://localhost:8080')
        .delete('/book/' + book._id)
        .end((err, res) => {
            expect(res).to.have.status(400);
            expect(res.body.message).to.equal('error deleting the book');
            done();
        })
})
