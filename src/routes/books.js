import fs from 'fs';
import path from 'path';
import uuid from 'uuid/v4';

import books from '../models/Book';

/*
 * GET /book route to retrieve all books.
 */
const getBooks = (req, res) => {
  books.find({}, (err,data) => {
    res.json(extractData(err, data));
  });
};

/*
 * POST /book to save a new book.
 */
const postBook = (req, res) => {
  let book = new books(req.body);
  book.save((err,data) => {
    res.json(extractData(err, { message: 'Book successfully added', data: data }))
  });
};

/*
 * GET /book/:id route to retrieve a book given its id.
 */
const getBook = (req, res) => {
  books.findById(req.params.id, (err, data) => {
    res.json(extractData(err, { message: 'Book fetched', data: data }));
  });
};

/*
 * PUT /book/:id to updatea a book given its id
 */
const updateBook = (req, res) => {
  books.findByIdAndUpdate(req.params.id, req.body, {new : true}, (err, data) => {
    res.json(extractData(err, { message: 'Book successfully updated', data: data }))
  });
};

/*
 * DELETE /book/:id to delete a book given its id.
 */
const deleteBook = (req, res) => {
  books.remove({_id: req.params.id}, (err, data) => {
    if (err)
      res.json(err);
    res.json({'message': 'Book successfully deleted'});
  });

};

function extractData(err, data) {
  if (err)
    return err;
  return data;
}

//export all the functions
export default { getBooks, postBook, getBook, deleteBook, updateBook };
